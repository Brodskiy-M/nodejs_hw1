const fs = require('fs');
const path = require('path');

const directoryPath = path.join(__dirname, '/files');
const allowedFormats = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];

const accessLogStream = fs.createWriteStream(
  path.join(__dirname, 'access.log'),
  { flags: 'a' }
);

function createFile(req, res, next) {
  const { filename, content } = req.body;
  if (!filename || typeof filename !== 'string') {
    return res
      .status(400)
      .send({ message: "Please specify valid 'filename' parameter" });
  }

  if (!content || typeof content !== 'string') {
    return res
      .status(400)
      .send({ message: "Please specify valid 'content' parameter" });
  }

  const fileExtension = path.extname(filename);
  if (!fileExtension) {
    return res.status(400).send({ message: 'Please write correct filename' });
  }

  if (!allowedFormats.includes(fileExtension)) {
    return res.status(400).send({
      message:
        "Please use filename with one of these extension: 'log', 'txt', 'json', 'yaml', 'xml', 'js'"
    });
  }

  const filePath = path.join(directoryPath, filename);
  fs.writeFileSync(filePath, JSON.stringify(content));
  res.status(200).send({ message: 'File created successfully' });
}

function getFiles(req, res, next) {
  const files = fs.readdirSync(directoryPath);

  if (!files) {
    return res.status(400).send({ message: 'Client error' });
  }

  res.status(200).send({
    message: 'Success',
    files: files
  });
}

function getFile(req, res, next) {
  const { filename } = req.params;
  const filePath = path.join(directoryPath, filename);

  if (!fs.existsSync(filePath)) {
    return res
      .status(400)
      .send({ message: `No file with '${filename}' filename found` });
  }

  if (!filename || typeof filename !== 'string') {
    return res
      .status(400)
      .send({ message: "Please specify valid 'filename' parameter" });
  }

  const fileContent = fs.readFileSync(filePath, 'utf-8');
  const fileExtension = path.extname(filename).slice(1);
  const fileDate = fs.statSync(filePath).birthtime;

  res.status(200).send({
    message: 'Success',
    filename: filename,
    content: JSON.parse(fileContent),
    extension: fileExtension,
    uploadedDate: fileDate
  });
}

function editFile(req, res, next) {
  const { filename } = req.params;
  const { content } = req.body;
  const filePath = path.join(directoryPath, filename);

  if (!fs.existsSync(filePath)) {
    return res
      .status(400)
      .send({ message: `No file with ${filename} filename found` });
  }

  if (!content) {
    return res.status(400).send({ message: 'Please add new content' });
  }

  fs.writeFileSync(filePath, JSON.stringify(content));

  res.status(200).send({
    message: 'Success',
    filename: filename,
    newContent: content
  });
}

function deleteFile(req, res, next) {
  const { filename } = req.params;
  const filePath = path.join(directoryPath, filename);

  if (!fs.existsSync(filePath)) {
    return res
      .status(400)
      .send({ message: `No file with ${filename} filename found` });
  }

  fs.unlinkSync(filePath);

  res.status(200).send({
    message: `File ${filename} was deleted`
  });
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile,
  accessLogStream
};
